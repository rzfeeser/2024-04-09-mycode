terraform {
  required_providers {
    docker = {
      source  = "registry.terraform.io/kreuzwerker/docker"
      version = "~> 2.22.0"
    }
  }
}

provider "docker" {}
