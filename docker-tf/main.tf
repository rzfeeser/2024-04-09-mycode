terraform {
  required_providers {
    docker = {
      source  = "registry.terraform.io/kreuzwerker/docker"
      version = "~> 2.22.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "wallawallawashington" {
  name         = "docker.io/nginx:1.19.6"
  keep_locally = true       // keep image after "destroy"
}

resource "docker_container" "nginx" {
  image = docker_image.wallawallawashington.image_id
  name  = "zachcontainer"
  ports {
    internal = 80
    external = 8000
  }
}

